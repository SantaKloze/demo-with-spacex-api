// latest flight form spaceX api :
// https://api.spacexdata.com/v3/launches/latest
$(document).ready(function() {
  var limitNbr = 3;
  var maxLaunch = 0;
  $.ajax({
    url:'https://api.spacexdata.com/v3/launches',
    type: 'GET',
    data: {

    },
    success: (success) => {
      maxLaunch = success.length;
      maxLaunch = maxLaunch - 3;
    },
    error: (error) => {
      console.log(error);
    }
  });

  $.ajax({
    url:'https://api.spacexdata.com/v3/launches/past',
    type: 'GET',
    data: {
      limit: limitNbr,
      sort: 'launch_date_utc',
      order: 'desc'
    },
    success: (success) => {
      var nbrLaunch = 0
      success.forEach(element => {
        var missionName = element.mission_name;
        var launch_site = element.launch_site.site_name_long;
        var articleLink = element.links.article_link;
        var firstFlightImage = element.links.flickr_images[0];
        $('#fillCarousel').append(`
          <div class="item">

            <div id="leftElements" class="element">
              <img id="frstMImg" src="` +  firstFlightImage + `" alt="This SpaceX Mission's First Image">
            </div>
            <div id="rightElements" class="element">
              <p id="missionTitleName" class="titleFont">Mission Name :<br><span id="missionTitle">` + missionName + `</span></p>
              <p id="missionSiteName" class="titleFont">Mission Launch Site:<br><span id="missionSite">` + launch_site + `</span></p>
              
              <span id="articleLink"><a id="articleLinkHref" href="` + articleLink + `">Lien vers l'article de la mission</a></span>

            </div>
          </div>`
        );
      });
    },
    error: response => {
      console.log(response)
    }
  });

  $('#plusButton').click(function() {
    console.log('AH');
    maxLaunch = maxLaunch - 1;
    console.log(maxLaunch);
    $('#fillCarousel').remove('.item');
    $.ajax({
      url:'https://api.spacexdata.com/v3/launches/' + maxLaunch,
      // url:'https://api.spacexdata.com/v3/launches/',
      type: 'GET',
      success: (success) => {
        console.log(success);
        var nbrLaunch = 0
          var missionName = success.mission_name;
          var launch_site = success.launch_site.site_name_long;
          var articleLink = success.links.article_link;
          var firstFlightImage = success.links.flickr_images[0];
          var imgHtmlElement =
            `<div id="leftElements" class="element">
              <img id="frstMImg" src="` +  firstFlightImage + `" alt="This SpaceX Mission's First Image">
            </div>`;
          var linkHtmlElement = 
            `<span id="articleLink"><a id="articleLinkHref" href="` + articleLink + `">Lien vers l'article de la mission</a></span>`;

          if (!articleLink) {
            linkHtmlElement = 
            `<span id="articleLink"><a id="articleLinkHref">Pas d'article lié à la mission</a></span>`;
          }
          if (!firstFlightImage){
            imgHtmlElement =
            `<div id="leftElements" class="element">
              <p style="color: white; font-size: 35px; width: 300px; text-align: center;">Pas d'image de la mission.</p>
            </div>`;
          }

          $('#fillCarousel').append(`
            <div class="item">`
             + imgHtmlElement +
              `<div id="rightElements" class="element">
                <p id="missionTitleName" class="titleFont">Mission Name :<br><span id="missionTitle">` + missionName + `</span></p>
                <p id="missionSiteName" class="titleFont">Mission Launch Site:<br><span id="missionSite">` + launch_site + `</span></p>
                
                ` + linkHtmlElement +`
  
              </div>
            </div>`
          );
      },
      error: response => {
        console.log(response)
      }
    });
  });

});
